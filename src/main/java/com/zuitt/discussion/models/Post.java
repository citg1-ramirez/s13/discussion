package com.zuitt.discussion.models;
import javax.persistence.*;

//Marks this java object as a representation of an entity/record from the database table "posts".
@Entity
//Designate the table name related to the model.
@Table(name = "posts")
public class Post {

    //Properties
    //Indicates the primary key
    @Id
    //Id will be auto-incemented.
    @GeneratedValue
    private Long id;
    //Class properties that represents table column ina  relational database are annoted as @Column
    @Column
    private String title;
    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public Post() {}

    public Post(Long id, String title, String content, User user) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
