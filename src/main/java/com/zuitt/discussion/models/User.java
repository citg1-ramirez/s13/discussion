package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    //Establishes the relationship of the property to the "user" model.
    @OneToMany(mappedBy = "user")
    //Prevents infinite recursion with bidirectional relationship.
    @JsonIgnore
    //"Set" class is a collection that contains no duplicate elements.
    private Set<Post> posts;

    public User() {}

    public User(Long id, String username, String password, Set<Post> posts) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.posts = posts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }
}
