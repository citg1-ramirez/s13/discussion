//Contains the business logic concerned with a particular object in the class.
package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class PostServiceImpl implements PostService{

    @Autowired
    PostRepository postRepository;

    @Override
    public void createPost(Post post) {
        postRepository.save(post);
    }

    @Override
    public Iterable getPosts() {
        return postRepository.findAll();
    }

    @Override
    public ResponseEntity deletPost(Long postid) {
        postRepository.deleteById(postid);
        return new ResponseEntity("Post deleted successfully", HttpStatus.OK);
    }

    @Override
    public ResponseEntity updatePost(Long postid, Post post) {
        Post postForUpdate = postRepository.findById(postid).get();

        postForUpdate.setTitle((post.getTitle()));
        postForUpdate.setContent(post.getContent());

        postRepository.save(postForUpdate);

        return new ResponseEntity("Post updated successfully", HttpStatus.OK);
    }
}
