package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;

import java.util.Optional;

public interface UserService {

    //Optional - defines if the method may/may not return an object of the User class.
    Optional<User> findByUsername(String username);
}
