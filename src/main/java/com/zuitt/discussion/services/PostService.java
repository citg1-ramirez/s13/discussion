//Service is an interface that exposes the methods of an implementation whose details that has been abstracted away.

package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(Post post);

    Iterable getPosts();

    ResponseEntity deletPost(Long postid);

    ResponseEntity updatePost (Long postid, Post post);
}
