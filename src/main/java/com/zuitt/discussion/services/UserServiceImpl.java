package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class UserServiceImpl implements  UserService{

    @Autowired
    private UserRepository userRepository;

    //Find user by username
    @Override
    //if findByUsername method returns null it will throw a NullPointerException.
    //using .ofNullable method will avoid this from happening.

    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
}
